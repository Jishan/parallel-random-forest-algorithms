#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn import tree
import multiprocessing as mp
import timeit
from sklearn.ensemble import RandomForestClassifier


# In[ ]:


def generate_iris(num_features=4, training_size=0.8):
    iris = load_iris()
    ##################################
    df = pd.DataFrame({
        'sepal_length': iris.data[:, 0],
        'sepal_width': iris.data[:, 1],
        'petal_length': iris.data[:, 2],
        'petal_width': iris.data[:, 3],
        'species': iris.target})
    X = df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']]
    y = df['species']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=(1-training_size), shuffle=True)
    return X_train, X_test, y_train, y_test


# In[ ]:


def generate_HIGGS(num_features=19, training_size=0.8):   #implemented an optional input if desired to simulate smaller sample sizes/features
    df = pd.read_csv('atlas-higgs-challenge-2014-v2.csv')
    df[df == -999.0] = np.nan
    df.dropna(axis=1, inplace=True)
    #df = df.fillna(0)   #alternative to dropna
    X = df.drop(columns=['EventId', 'Weight', 'Label', 'KaggleSet', 'KaggleWeight'])
    X = X.drop(columns=X.columns[num_features:])
    keepLabel = list(df.columns)
    keepLabel.remove("Label")
    y = df.drop(columns=keepLabel)   #keep the label
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=(1-training_size), shuffle=True)
    return X_train, X_test, y_train, y_test


# In[ ]:


def sklearn_RandomForest(X, y, X_tst, y_tst, trees, depths, criteria, jobs, max_feas):
    acc_score = []
    clf = RandomForestClassifier(n_estimators = trees, max_depth = depths, n_jobs = jobs, criterion = criteria, max_features = max_feas) #Didn't use bootstrap, as it was already true.
    clf.fit(X, y.values.ravel()) 
    acc_score = clf.score(X_tst[:1000], y_tst[:1000])   # limit scoring for benchmarking speed
    return acc_score


# In[ ]:


def format_output(run_number, training_time, cores, data_size, trees, max_depth, score, feature_tree, criterion, width):
    return "{},{},{},{},{},{},{},{},{},{}\n".format(run_number, training_time, cores, data_size, trees,
                                                       max_depth, score, feature_tree, criterion, width)


# In[ ]:


def main():
    total_time_start = timeit.default_timer()
    CRITERION = 'gini'         # 'gini' or 'entropy'
    VERBOSE = True             # set to False to turn off print statements
    if VERBOSE:
        print("Welcome to our sklearn random forest built with n_jobs support")
    # TEST PARAMETERS
    core_list = [1, 4, 8]
    num_trees = [1, 8, 64]
    features_per_split = ['sqrt']     # ['sqrt', 'log2', None]  # Can also use int to specify a number
    max_depths = [1, 10, 100]
    data_num = [0.01, 0.4, 0.8]
    data_features = [4, 9, 19]
    run_count = 1
    results = []
    with open('results_sklearn_run10.csv', 'w') as myResults:
        myResults.write('Run Number,Training Time,Num Cores,Num Datapoints,Num Trees,Max Depth,Score,FeaturesPerTree,Criterion,Num_features\n')


    for width in data_features:
        for data in data_num:
            if VERBOSE:
                print("Processing HIGGS dataset")
            X_train, X_test, y_train, y_test = generate_HIGGS(num_features=width, training_size=data)  # Load the data
            #X_train, X_test, y_train, y_test = generate_iris(num_features=width, training_size=data)  # iris useful for testing quicker
            n_samples = len(X_train)
            #n_features = len(X_train.columns)
            # print("HIGGS dataset ready")
            for cores in core_list:
                for TREES in num_trees:
                    for FEATURE_SELECT in features_per_split:
                        for DEPTH in max_depths:
                            # Start of the run
                            start = timeit.default_timer()  # Timing the runs
                            if VERBOSE:
                                print("Starting training run #{}/243 for {} core(s) making {} tree(s), data size {} x {}".format(run_count, cores, TREES, n_samples, width))
                            results = [sklearn_RandomForest(X_train, y_train, X_test, y_test, TREES, DEPTH, CRITERION, cores, FEATURE_SELECT)]
                            elapsed = timeit.default_timer() - start  # stop the timer
                            if VERBOSE:
                                print('Training time is: {:.3} seconds'.format(elapsed))
                         
                            output_entry = format_output(run_count, elapsed, cores, n_samples, TREES, DEPTH,
                                                         results[0], FEATURE_SELECT, CRITERION, width)
                            with open('results_sklearn_run10.csv', 'a') as myResults:
                                myResults.write(output_entry)
                            run_count += 1
    total_elapsed = timeit.default_timer() - total_time_start
    with open('total_time_sklearn_run10.txt', 'w') as myTimer:
        myTimer.write(str(total_elapsed))


if __name__ == '__main__':
    main()




