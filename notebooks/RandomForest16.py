import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn import metrics
from sklearn import tree
import multiprocessing as mp
import timeit
from scipy import stats


# At 500 trees, expect a single processor to take about an hour.

# Things to do:
# Parallelize the predict function? (if we want to benchmark it)
# DONE - Develop loop to develop all the data points we want
# DONE - Output/save run data into an output file
# Do we want any command line arguments?

# DATASET LOADING

# Iris included for testing.  Not currently in use.
def generate_iris():
    iris = load_iris()
    ##################################
    df = pd.DataFrame({
        'sepal_length': iris.data[:, 0],
        'sepal_width': iris.data[:, 1],
        'petal_length': iris.data[:, 2],
        'petal_width': iris.data[:, 3],
        'species': iris.target})
    X = df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']]
    y = df['species']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)
    return X_train, X_test, y_train, y_test


def generate_HIGGS(num_features=19, training_size=0.8):   #implemented an optional input if desired to simulate smaller sample sizes/features
    df = pd.read_csv('atlas-higgs-challenge-2014-v2.csv')
    df[df == -999.0] = np.nan
    df.dropna(axis=1, inplace=True)
    #df = df.fillna(0)   #alternative to dropna
    X = df.drop(columns=['EventId', 'Weight', 'Label', 'KaggleSet', 'KaggleWeight'])
    X = X.drop(columns=X.columns[num_features:])
    keepLabel = list(df.columns)
    keepLabel.remove("Label")
    y = df.drop(columns=keepLabel)   #keep the label
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=(1-training_size), shuffle=True)
    return X_train, X_test, y_train, y_test
# END DATASET LOADING


# NON PARALLEL IMPLEMENTATION
class random_forest_classifier:
    def __init__(self, n_trees=10, max_depth=10, criterion='gini',
                 tree_features='sqrt'):  # could give these all default values, probably missing some in current list
        self.trees = []  # stores all the trees in the forest
        self.n_trees = n_trees  # how many trees in the forest
        self.n_bootstrap = 1  # set to bootstrap data up to the original size of X
        # self.n_features = n_features   #removed and redefined, number of features auto detected during build_forest, the sqrt(n_features) used for individual trees
        self.max_depth = max_depth  # the maximum depth a tree should be
        self.criterion = criterion  # 'gini' or 'entropy'
        self.tree_features = tree_features  # 'sqrt' or 'p_2' or 'p'

    def get_params(self,
                   deep=True):  # This does nothing useful right now, was trying to make it sklearn compatible for cross_val_score and the like
        parameters = {}
        parameters['n_trees'] = self.n_trees
        parameters['max_depth'] = self.max_depth
        parameters['trees'] = self.trees
        parameters['n_bootstrap'] = self.n_bootstrap
        parameters['criterion'] = self.criterion
        parameters['tree_features'] = self.tree_features
        try:
            parameters['n_features'] = self.n_features
        except:
            pass

    def build_tree(self, X, y):
        Tree = tree.DecisionTreeClassifier(max_depth=self.max_depth,
                                           criterion=self.criterion)  # need to decide what parameters/options to feed it, gini vs entropy
        if self.tree_features == 'sqrt':
            tree_features = int(np.sqrt(self.n_features))
        elif self.tree_features == 'p_2':
            tree_features = int(self.n_features / 2)
        elif self.tree_features == 'p':
            tree_features = int(self.n_features)
        else:
            print('Invalid tree_features parameter, defaulting to sqrt')
            tree_features = int(np.sqrt(self.n_features))

        X_sample = X.sample(tree_features,
                            axis=1)  # this performs the feature sampling, is sqrt(n_features) ok? or do we want to define the function?
        Tree.fit(X_sample, y)
        return (Tree, X_sample.columns)  # needed to also store what features to send the tree for predictions

    def build_forest(self, X, y):
        self.n_features = len(X.columns)
        current_trees = 0
        while current_trees < self.n_trees:
            X_sample = X.sample(frac=self.n_bootstrap,
                                replace=True)  # assuming n_bootstrap is the fraction of the orignal sample size. change to n= if you want to set an integer
            self.trees.append(self.build_tree(X_sample, y[X_sample.index]))
            current_trees += 1

    def fit(self, X, y):
        self.build_forest(X, y)

    def predict(self, X):
        predictions = pd.DataFrame(columns=np.arange(0, self.n_trees))  # each tree a column
        for t in range(self.n_trees):
            predictions.iloc[:, t] = self.trees[t][0].predict(
                X[self.trees[t][1]])  # probably a more efficient way than this
        results = predictions.mode(axis=1)
        return results.iloc[:, 0]  # added the iloc for the edge case of multiple modes, just arbitrarily pick one

    def score(self, X, y):  # currently just calculating accuracy
        y_pred = self.predict(X)
        compare = np.asarray(y) == np.asarray(
            y_pred)  # need to use asarray to deal with lack of column name in y_pred, d_type, and index
        return np.mean(compare)

# 5-Fold is not needed.  Non parallel implementation currently not in use.
# criterion: 'gini' or 'entropy'; tree_features: 'sqrt' 'p_2' 'p'
def run_single_forest(X, y, n_trees, max_depth, criterion, tree_features):
    k_fold = 5
    skf = StratifiedKFold(n_splits=k_fold, shuffle=True)
    k = 1
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        myForest = random_forest_classifier(n_trees=n_trees, max_depth=max_depth, criterion=criterion,
                                            tree_features=tree_features)
        myForest.fit(X_train, y_train)
        print("Fold {} accuracy score is {:.3}".format(k, myForest.score(X_test, y_test)))
        k += 1

# END OF NON PARALLEL IMPLEMENTION


# PARALLEL IMPLEMENTATION
class random_forest_classifier_parallel:
    def __init__(self, n_trees=10, max_depth=10, n_features=0,
                 criterion='gini', max_features='sqrt'):  # could give these all default values, probably missing some in current list
        self.trees = []  # stores all the trees in the forest
        self.n_trees = n_trees  # how many trees in the forest
        self.n_bootstrap = 1  # set to bootstrap data up to the original size of X
        self.n_features = n_features   #removed and redefined, number of features auto detected during build_forest
        self.max_depth = max_depth  # the maximum depth a tree should be
        self.criterion = criterion  # 'gini' or 'entropy'
        self.max_features = max_features

    def build_tree(self, X, y):
        Tree = tree.DecisionTreeClassifier(max_depth=self.max_depth,
                                           criterion=self.criterion,
                                           max_features=self.max_features)
        Tree.fit(X, y)
        return Tree

    def build_forest(self, X, y):
        self.n_features = len(X.columns)
        current_trees = 0
        while current_trees < self.n_trees:
            X_sample = X.sample(frac=self.n_bootstrap,
                                replace=True)
            self.trees.append(self.build_tree(X_sample, y[X_sample.index]))
            current_trees += 1

    def fit(self, X, y, restart=True):
        if restart:
            self.trees = []   # clear the old tree list
        self.build_forest(X, y)

    def add_trees(self, trees):
        self.trees.extend(trees)

    def predict(self, X):
        n_trees = len(self.trees)
        predictions = []
        for t in range(n_trees):
            predictions.append(self.trees[t].predict(X))
        predictions = np.array(predictions)
        results = stats.mode(predictions, axis=0)
        return results[0][0]

    def score(self, X, y):  # currently just calculating accuracy
        y_pred = self.predict(X)
        compare = np.asarray(y) == np.asarray(y_pred.reshape(y.shape))
        # need to use asarray to deal with lack of column name in y_pred, d_type,
        # and index and reshape to ensure same dimensions
        return np.mean(compare)

    def scoreF1(self, X, y):
        y_pred = self.predict(X)
        return metrics.f1_score(y, y_pred)

# Made an external build_tree function for parallelization
def build_tree(X, y, n_bootstrap, max_depth, criterion, features_selection):
    Tree = tree.DecisionTreeClassifier(max_depth=max_depth, criterion=criterion, max_features=features_selection)
    X_boot = X.sample(frac=n_bootstrap, replace=True)
    y_boot = y.loc[X_boot.index]
    Tree.fit(X_boot, y_boot)
    return Tree


def format_output(run_number, training_time, cores, data_size, trees, max_depth, score, feature_tree, criterion, width):
    return "{},{},{},{},{},{},{},{},{},{}\n".format(run_number, training_time, cores, data_size, trees,
                                                       max_depth, score, feature_tree, criterion, width)

# Most of the multiprocessing magic happens here
def main():
    total_time_start = timeit.default_timer()
    CRITERION = 'gini'         # 'gini' or 'entropy'
    VERBOSE = True             # set to False to turn off print statements
    if VERBOSE:
        print("Welcome to our random forest implementation with multiprocessing support")
    # TEST PARAMETERS
    core_list = [16]
    num_trees = [64]
    features_per_split = ['sqrt']     # ['sqrt', 'log2', None]  # Can also use int to specify a number
    max_depths = [100]
    data_num = [0.8]
    data_features = [19]
    run_count = 1
    with open('results.csv', 'w') as myResults:
        myResults.write('Run Number,Training Time,Num Cores,Num Datapoints,Num Trees,Max Depth,Score,FeaturesPerTree,Criterion,Num_features\n')

    for i in range(10):
        for width in data_features:
            for data in data_num:
                if VERBOSE:
                    print("Processing HIGGS dataset")
                X_train, X_test, y_train, y_test = generate_HIGGS(num_features=width, training_size=data)  # Load the data
                # X_train, X_test, y_train, y_test = generate_iris()  # iris useful for testing quicker
                n_samples = len(X_train)
                n_features = len(X_train.columns)
                # print("HIGGS dataset ready")
                for cores in core_list:
                    for TREES in num_trees:
                        for FEATURE_SELECT in features_per_split:
                            for DEPTH in max_depths:
                                # Start of the run
                                start = timeit.default_timer()  # Timing the runs
                                if VERBOSE:
                                    print("Starting training run #{}/243 for {} core(s) making {} tree(s), data size {} x {}".format(run_count, cores, TREES, n_samples, width))
                                myRF = random_forest_classifier_parallel(n_trees=TREES, max_depth=DEPTH, n_features=n_features, criterion=CRITERION)
                                pool = mp.Pool(cores)   # This starts the multiprocessing magic
                                tree_results = [pool.apply_async(build_tree, args=(X_train, y_train, 1, DEPTH, CRITERION, FEATURE_SELECT)) for i in range(TREES)]
                                pool.close()
                                pool.join()  # and the multiprocessing is over...
                                trees = []
                                for item in tree_results:
                                    trees.append(item.get())
                                myRF.add_trees(trees)  # add the trees/forest generated to our model
                                elapsed = timeit.default_timer() - start  # stop the timer
                                if VERBOSE:
                                    print('Training time is: {:.3} seconds'.format(elapsed))
                                score = myRF.score(X_test[:1000], y_test[:1000])     # limit scoring for benchmarking speed
                                #score = np.nan
                                if VERBOSE:
                                    print("Model Error Rate is: {:.4}".format(1 - score))
                                # score (for mean accuracy) for scoreF1 (for F1 score)
                                output_entry = format_output(run_count, elapsed, cores, n_samples, TREES, DEPTH,
                                                             score, FEATURE_SELECT, CRITERION, width)
                                with open('results.csv', 'a') as myResults:
                                    myResults.write(output_entry)
                                run_count += 1
    total_elapsed = timeit.default_timer() - total_time_start
    with open('total_time.txt', 'w') as myTimer:
        myTimer.write(str(total_elapsed))


if __name__ == '__main__':
    main()