##########################################################################
# To improve efficiency the algorithm converts dataframe to 2d array and work only on arrays 
# The label column of the testing dataframe must be in the last column
##########################################################################
import numpy as np
import pandas as pd


def check_purity(data): # checks whether all the data is pure (have the same label) 
    labels = data[:, -1]  # the last column is the label
    unique_classes = np.unique(labels)
    if len(unique_classes) == 1:
        return True
    else:
        return False
    
###############################################################
        
def classify_data(data): # voting for classification
    labels = data[:, -1]
    #return numer of unique classes and count for each
    unique_classes, counts_unique_classes = np.unique(labels, return_counts=True)
    index = counts_unique_classes.argmax()
    classification = unique_classes[index]
    return classification

###############################################################
    
def get_potential_splits(data): # calculate potential splits at each node of the tree
    potential_splits = {}
    for column_index in range(data.shape[1] - 1):        # excluding the last column which is the label
        potential_splits[column_index] = []
        values = data[:, column_index]
        unique_values = np.unique(values)
        #to create splits
        for index in range(len(unique_values)):
            if index != 0:
                current_value = unique_values[index]
                previous_value = unique_values[index - 1]
                potential_split = (current_value + previous_value) / 2
                potential_splits[column_index].append(potential_split)
    return potential_splits

###############################################################
    
def split_data(data, split_column, split_value): #returns data below and above the split
    split_column_values = data[:, split_column]
    data_below = data[split_column_values <= split_value]
    data_above = data[split_column_values >  split_value]
    return data_below, data_above

###############################################################
    
def calculate_entropy(data):
    labels = data[:, -1]
    unique_classes, counts_unique_classes = np.unique(labels, return_counts=True)
    probabilities = counts_unique_classes / counts_unique_classes.sum()
    entropy = sum(probabilities * -np.log2(probabilities)) 
    return entropy

###############################################################
    
def calculate_gini(data):
    label_column = data[:, -1]
    _, counts = np.unique(label_column, return_counts=True)
    probabilities = counts / counts.sum()
    gini = 1-sum(probabilities * probabilities) 
    return gini

###############################################################
    
def calculate_overall_metric(data_below, data_above, metric_function):
    n = len(data_below) + len(data_above)
    p_data_below = len(data_below) / n
    p_data_above = len(data_above) / n
    overall_metric =  (p_data_below * metric_function(data_below) + p_data_above * metric_function(data_above))
    return overall_metric

###############################################################
    
def determine_best_split(data, potential_splits, metric_function):
    overall_entropy = 9999   #setting the initial split value to an arbitrary large number
    for column_index in potential_splits:                       
        for value in potential_splits[column_index]:
            data_below, data_above = split_data(data, split_column=column_index, split_value=value)
            if metric_function == "entropy":
                current_overall_metric = calculate_overall_metric(data_below, data_above, metric_function=calculate_entropy)
            if metric_function == "gini":
                current_overall_metric = calculate_overall_metric(data_below, data_above, metric_function=calculate_gini)

            if current_overall_metric <= overall_entropy:
                overall_entropy = current_overall_metric
                best_split_column = column_index
                best_split_value = value
    return best_split_column, best_split_value

###############################################################

def decision_tree_algorithm(df, counter=0, min_samples=2, max_depth=5, metric_function = 'entropy'):
    # data preparations
    if counter == 0:
        global headers
        headers = df.columns
        data = df.values
    else:
        data = df        
    
    # in the base case checks whether are labels are pure, if so, return classification and stop
    if (check_purity(data)) or (len(data) < min_samples) or (counter == max_depth):
        classification = classify_data(data)    
        return classification

    
    # recursive part
    else:    
        counter += 1
        potential_splits = get_potential_splits(data)
        split_column, split_value = determine_best_split(data, potential_splits, metric_function)
        data_below, data_above = split_data(data, split_column, split_value)
        
        # instantiate subtree
        feature_name = headers[split_column]
        question = "{} <= {}".format(feature_name, split_value)
        sub_tree = {question: []}
        
        # find answers (recursion)
        yes_answer = decision_tree_algorithm(data_below, counter, min_samples, max_depth)
        no_answer = decision_tree_algorithm(data_above, counter, min_samples, max_depth)
        
        # If the answers are the same, then there is no point in asking the qestion.
        # This could happen when the data is classified even though it is not pure
        if yes_answer == no_answer:
            sub_tree = yes_answer
        else:
            sub_tree[question].append(yes_answer)
            sub_tree[question].append(no_answer)
        
        return sub_tree
    
##############################################################
        
#make predictions for one example
def predict_example(example, tree):
    
    # tree is just a root node
    if not isinstance(tree, dict):
        return tree
    
    question = list(tree.keys())[0]
    feature_name, comparison_operator, value = question.split(" ")
    # ask question
    if comparison_operator == "<=":
        if example[feature_name] <= float(value):
            answer = tree[question][0]
        else:
            answer = tree[question][1]    
    # feature is categorical
    else:
        if str(example[feature_name]) == value:
            answer = tree[question][0]
        else:
            answer = tree[question][1]
    # base case
    if not isinstance(answer, dict):
        return answer    
    # recursive part
    else:
        residual_tree = answer
        return predict_example(example, residual_tree)

##################################################################
        
def make_predictions(df, tree):
    
    if len(df) != 0:
        predictions = df.apply(predict_example, args=(tree,), axis=1)
    else:
        predictions = pd.Series()
    return predictions
#####################################################################
    
def score(df, tree):
    predictions = make_predictions(df, tree)
    predictions_correct = predictions == df.label
    accuracy = predictions_correct.mean()
    return accuracy