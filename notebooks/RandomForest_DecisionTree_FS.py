import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.model_selection import StratifiedKFold
from sklearn import metrics
from sklearn import tree
import multiprocessing as mp
import timeit
from scipy import stats

#Desicion Tree Implemetation
#############################################################
def check_purity(data): # checks whether all the data is pure (have the same label) 
    labels = data[:, -1]  # the last column is the label
    unique_classes = np.unique(labels)
    if len(unique_classes) == 1:
        return True
    else:
        return False
    
###############################################################
        
def classify_data(data): # voting for classification
    labels = data[:, -1]
    #return numer of unique classes and count for each
    unique_classes, counts_unique_classes = np.unique(labels, return_counts=True)
    index = counts_unique_classes.argmax()
    classification = unique_classes[index]
    return classification

###############################################################
    
def get_potential_splits(data): # calculate potential splits at each node of the tree
    potential_splits = {}
    for column_index in range(data.shape[1] - 1):        # excluding the last column which is the label
        potential_splits[column_index] = []
        values = data[:, column_index]
        unique_values = np.unique(values)
        #to create splits
        for index in range(len(unique_values)):
            if index != 0:
                current_value = unique_values[index]
                previous_value = unique_values[index - 1]
                potential_split = (current_value + previous_value) / 2
                potential_splits[column_index].append(potential_split)
    return potential_splits

###############################################################
    
def split_data(data, split_column, split_value): #returns data below and above the split
    split_column_values = data[:, split_column]
    data_below = data[split_column_values <= split_value]
    data_above = data[split_column_values >  split_value]
    return data_below, data_above

###############################################################
    
def calculate_entropy(data):
    labels = data[:, -1]
    unique_classes, counts_unique_classes = np.unique(labels, return_counts=True)
    probabilities = counts_unique_classes / counts_unique_classes.sum()
    entropy = sum(probabilities * -np.log2(probabilities)) 
    return entropy

###############################################################
    
def calculate_gini(data):
    label_column = data[:, -1]
    _, counts = np.unique(label_column, return_counts=True)
    probabilities = counts / counts.sum()
    gini = 1-sum(probabilities * probabilities) 
    return gini

###############################################################
    
def calculate_overall_metric(data_below, data_above, metric_function):
    n = len(data_below) + len(data_above)
    p_data_below = len(data_below) / n
    p_data_above = len(data_above) / n
    overall_metric =  (p_data_below * metric_function(data_below) + p_data_above * metric_function(data_above))
    return overall_metric

###############################################################
    
def determine_best_split(data, potential_splits, metric_function):
    overall_entropy = 1e4   #setting the initial split value to an arbitrary large number
    #print("potential splits: {}".format(potential_splits))
    for column_index in potential_splits:                       
      for value in potential_splits[column_index]:
        data_below, data_above = split_data(data, split_column=column_index, split_value=value)
        if metric_function == "entropy":
          current_overall_metric = calculate_overall_metric(data_below, data_above, metric_function=calculate_entropy)
        if metric_function == "gini":
          current_overall_metric = calculate_overall_metric(data_below, data_above, metric_function=calculate_gini)

        if current_overall_metric <= overall_entropy:
          overall_entropy = current_overall_metric
          best_split_column = column_index
          best_split_value = value
        #else:
        #  print(current_overall_metric)
      
    return best_split_column, best_split_value

###############################################################

def decision_tree_algorithm(df, counter=0, min_samples=2, max_depth=5, metric_function = 'gini'):
    # data preparations
    # only for the first call of the function we need to change the df to 2d array since the output 
    # of the decision_tree_algorithm is a 2d array. For other recursive calls we do not need to convert to 2d again.

    if counter == 0:
        last_column = df.iloc[:, -1]
        df2 = df.iloc[:, :-1]
        n_features = df2.shape[1]
        max_features = int(np.sqrt(n_features))
        df_columns = df2.sample(max_features,axis=1)
        df_columns['label']=last_column

        global headers
        headers = df_columns.columns
        data = df_columns.values

    else:
        data = df     
    
    # in the base case checks whether are labels are pure, if so, return classification and stop
    if (check_purity(data)) or (len(data) < min_samples) or (counter == max_depth):
        classification = classify_data(data)    
        return classification
    
    # recursive part
    else:    
        counter += 1
<<<<<<< HEAD
=======
        print(counter)
>>>>>>> 8cd3d114eb59c1acd7f3ac70712babc684adb6f5
        potential_splits = get_potential_splits(data)
        split_column, split_value = determine_best_split(data, potential_splits, metric_function)
        data_below, data_above = split_data(data, split_column, split_value)
        
        # instantiate subtree
        feature_name = headers[split_column]
        question = "{} <= {}".format(feature_name, split_value)
        sub_tree = {question: []}
        
        # find answers (recursion)
        yes_answer = decision_tree_algorithm(data_below, counter, min_samples, max_depth)
        no_answer = decision_tree_algorithm(data_above, counter, min_samples, max_depth)
        
        # If the answers are the same, then there is no point in asking the qestion.
        # This could happen when the data is classified even though it is not pure
        if yes_answer == no_answer:
            sub_tree = yes_answer
        else:
            sub_tree[question].append(yes_answer)
            sub_tree[question].append(no_answer)
        
        return sub_tree

##############################################################
#make predictions for one example
def predict_example(example, tree):
    
    # tree is just a root node
    if not isinstance(tree, dict):
        return tree
    
    question = list(tree.keys())[0]
    feature_name, comparison_operator, value = question.split(" ")
    # ask question
    if comparison_operator == "<=":
        if example[feature_name] <= float(value):
            answer = tree[question][0]
        else:
            answer = tree[question][1]    
    # feature is categorical
    else:
        if str(example[feature_name]) == value:
            answer = tree[question][0]
        else:
            answer = tree[question][1]
    # base case
    if not isinstance(answer, dict):
        return answer    
    # recursive part
    else:
        residual_tree = answer
        return predict_example(example, residual_tree)

##################################################################
        
def make_predictions(df, tree):
    
    if len(df) != 0:
        predictions = df.apply(predict_example, args=(tree,), axis=1)
    else:
        predictions = pd.Series()
    return predictions
#####################################################################
    
def score(df, tree):
    predictions = make_predictions(df, tree)
    predictions_correct = predictions == df.label
    accuracy = predictions_correct.mean()
    return accuracy
<<<<<<< HEAD

=======
>>>>>>> 8cd3d114eb59c1acd7f3ac70712babc684adb6f5
####################################################################
####################################################################
####################################################################
#Random Forest Implementation
# At 500 trees, expect a single processor to take about an hour.
# PARALLEL IMPLEMENTATION
class random_forest_classifier_parallel:
    def __init__(self, n_trees=10, max_depth=10, n_features=0,
                 criterion='gini', max_features='sqrt'):  # could give these all default values, probably missing some in current list
        self.trees = []  # stores all the trees in the forest
        self.n_trees = n_trees  # how many trees in the forest
        self.n_bootstrap = 1  # set to bootstrap data up to the original size of X
        self.n_features = n_features   #removed and redefined, number of features auto detected during build_forest
        self.max_depth = max_depth  # the maximum depth a tree should be
        self.criterion = criterion  # 'gini' or 'entropy'
        self.max_features = max_features

    def build_tree(self, X, y):
        Tree = tree.DecisionTreeClassifier(max_depth=self.max_depth,
                                           criterion=self.criterion,
                                           max_features=self.max_features)
        Tree.fit(X, y)
        return Tree

    def build_forest(self, X, y):
        self.n_features = len(X.columns)
        current_trees = 0
        while current_trees < self.n_trees:
            X_sample = X.sample(frac=self.n_bootstrap,
                                replace=True)
            self.trees.append(self.build_tree(X_sample, y[X_sample.index]))
            current_trees += 1

    def fit(self, X, y, restart=True):
        if restart:
            self.trees = []   # clear the old tree list
        self.build_forest(X, y)

    def add_trees(self, trees):
        self.trees.extend(trees)

    def predict(self, X):
        n_trees = len(self.trees)
        predictions = []
        for t in range(n_trees):
            predictions.append(make_predictions(X, self.trees[t]))
        predictions = np.array(predictions)
        results = stats.mode(predictions, axis=0)
        return results[0][0]

    def score(self, X, y):  # currently just calculating accuracy
        y_pred = self.predict(X)
        compare = np.asarray(y) == np.asarray(y_pred.reshape(y.shape))
        # need to use asarray to deal with lack of column name in y_pred, d_type,
        # and index and reshape to ensure same dimensions
        return np.mean(compare)

    def scoreF1(self, X, y):
        y_pred = self.predict(X)
        return metrics.f1_score(y, y_pred)
        
####################################################################
# Made an external build_tree function for parallelization
def build_tree(X, y, n_bootstrap, max_depth, criterion, features_selection):
    X_boot = X.sample(frac=n_bootstrap, replace=True)
    y_boot = y.loc[X_boot.index]
    X_boot = X_boot.join(y_boot)
    Tree = decision_tree_algorithm(X_boot, max_depth=max_depth, metric_function=criterion)
    return Tree


def format_output(run_number, training_time, cores, data_size, trees, max_depth, score, feature_tree, criterion, width):
    return "{},{},{},{},{},{},{},{},{},{}\n".format(run_number, training_time, cores, data_size, trees,
                                                       max_depth, score, feature_tree, criterion, width)

####################################################################
####################################################################
####################################################################
# DATASET LOADING
# Iris included for testing.  Not currently in use.
def generate_iris():
    iris = load_iris()
    ##################################
    df = pd.DataFrame({
        'sepal_length': iris.data[:, 0],
        'sepal_width': iris.data[:, 1],
        'petal_length': iris.data[:, 2],
        'petal_width': iris.data[:, 3],
        'species': iris.target})
    X = df[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']]
    y = df['species']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, shuffle=True)
    return X_train, X_test, y_train, y_test


def generate_HIGGS(num_features=19, training_size=0.8):   #implemented an optional input if desired to simulate smaller sample sizes/features
    df = pd.read_csv('atlas-higgs-challenge-2014-v2.csv')
    df[df == -999.0] = np.nan
    df.dropna(axis=1, inplace=True)
    #df = df.fillna(0)   #alternative to dropna
    X = df.drop(columns=['EventId', 'Weight', 'Label', 'KaggleSet', 'KaggleWeight'])
    X = X.drop(columns=X.columns[num_features:])
    keepLabel = list(df.columns)
    keepLabel.remove("Label")
    y = df.drop(columns=keepLabel)   #keep the label
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=(1-training_size), shuffle=True)
    return X_train, X_test, y_train, y_test
# END DATASET LOADING
<<<<<<< HEAD

=======
>>>>>>> 8cd3d114eb59c1acd7f3ac70712babc684adb6f5
####################################################################
####################################################################
####################################################################
# Most of the multiprocessing magic happens here
def main():
    total_time_start = timeit.default_timer()
    CRITERION = 'gini'         # 'gini' or 'entropy'
    VERBOSE = True             # set to False to turn off print statements
    if VERBOSE:
        print("Welcome to our random forest implementation with multiprocessing support")
    # TEST PARAMETERS
    core_list = [1,4,8]
    num_trees = [1,8]
    features_per_split = ['sqrt']     # ['sqrt', 'log2', None]  # Can also use int to specify a number
    max_depths = [1, 10]
    data_num = [0.0001,0.001,0.01]
    data_features = [4,9,19]
    run_count = 1
    with open('results_RF_DT_FS.csv', 'w') as myResults:
        myResults.write('Run Number,Training Time,Num Cores,Num Datapoints,Num Trees,Max Depth,Score,FeaturesPerTree,Criterion,Num_features\n')


    for width in data_features:
        for data in data_num:
            if VERBOSE:
                print("Processing HIGGS dataset")
            X_train, X_test, y_train, y_test = generate_HIGGS(num_features=width, training_size=data)  # Load the data
            #X_train, X_test, y_train, y_test = generate_iris()  # iris useful for testing quicker
            n_samples = len(X_train)
            n_features = len(X_train.columns)
            # print("HIGGS dataset ready")
            for cores in core_list:
                for TREES in num_trees:
                    for FEATURE_SELECT in features_per_split:
                        for DEPTH in max_depths:
                            # Start of the run
                            start = timeit.default_timer()  # Timing the runs
                            if VERBOSE:
                                print("Starting training run #{}/243 for {} core(s) making {} tree(s), data size {} x {}".format(run_count, cores, TREES, n_samples, width))
                            myRF = random_forest_classifier_parallel(n_trees=TREES, max_depth=DEPTH, n_features=n_features, criterion=CRITERION)
                            pool = mp.Pool(cores)   # This starts the multiprocessing magic
                            tree_results = [pool.apply_async(build_tree, args=(X_train, y_train, 1, DEPTH, CRITERION, FEATURE_SELECT)) for i in range(TREES)]
                            pool.close()
                            pool.join()  # and the multiprocessing is over...
                            trees = []
                            for item in tree_results:
                                trees.append(item.get())
                            myRF.add_trees(trees)  # add the trees/forest generated to our model
                            elapsed = timeit.default_timer() - start  # stop the timer
                            if VERBOSE:
                                print('Training time is: {:.3} seconds'.format(elapsed))
                            score = myRF.score(X_test[:1000], y_test[:1000])     # limit scoring for benchmarking speed
                            #score = np.nan
                            if VERBOSE:
                                print("Model Error Rate is: {:.4}".format(1 - score))
                            # score (for mean accuracy) for scoreF1 (for F1 score)
                            output_entry = format_output(run_count, elapsed, cores, n_samples, TREES, DEPTH,
                                                         score, FEATURE_SELECT, CRITERION, width)
                            with open('results_RF_DT_FS.csv', 'a') as myResults:
                                myResults.write(output_entry)
                            run_count += 1
    total_elapsed = timeit.default_timer() - total_time_start
    with open('total_time_RF_DT_FS.txt', 'w') as myTimer:
        myTimer.write(str(total_elapsed))


if __name__ == '__main__':
    main()
