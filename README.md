# Title: Comparing Computational Complexity and Efficiency of Parallel Random Forest Algorithms Implemented  in Python and R


# Results

We implemented a parallel random forest (PRF) framework in Python using the multiprocessing module and the `scikit-learn` decision tree. A decision tree algorithm was also created from scratch and integrated into our PRF framework. We compared our implementation against the `scikit-learn` random forest, and two parallel random forest packages in `R`. The training times were measured for each implementation over a variety of data sizes and the multiprocessing efficiency was calculated. The single-core values were used to calculate the algorithmic complexity of our implementation. The highlights of our results can be summarized in a couple graphs: 

![Efficiency Graph](/Graphs/Efficiency.png) ![Training Time Graph](/Graphs/Training_Time.png)

As illustrated above, our implementation had an efficiency marginally better than the `scikit-learn` random forest and significantly better than the RFSRC package.  While our training time was the slowest on a single CPU, as the number of CPUs increased, we were able to beat `RFSRC`.

# Notes
* Our implementation of parallel random forest was written in Python and can be found in `RandomForest.py`
* The HIGGS dataset was downloaded from `http://opendata.cern.ch/record/328`

# Contributors 

Jishan Ahmed, John Bartocci, Shadi Moradi, Upeksha Perera, Deepak Thammineni
